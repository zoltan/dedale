# Dedale

[![pipeline status](https://gitlab.com/zoltan/dedale/badges/devel/pipeline.svg)](https://gitlab.com/zoltan/dedale/-/commits/devel)
[![coverage report](https://gitlab.com/zoltan/dedale/badges/devel/coverage.svg)](https://gitlab.com/zoltan/dedale/-/commits/devel)

Dedale is a game where players need to find the exit of a 2d maze.

## Concepts

> This section is a draft and needs to be rewritten.

The player is only given the distances to the walls on the left, on the right
and in front of them. The only actions available are turning left or right and
moving forward. The distances to the walls are given after every action.

The game ends when the player reaches the exit.

### Score

The player starts with a score of 0. Each action increase the score by X points.

### Actions

- move forward
- turn left / right

Every action return the distance to the wall on the left, in front and on the
right. moving cost points.

possible new actions:
- see through walls, see the 8 cells surrounding the player, cost points, can be
  extended to the surrounding 16 cells from more points etc
- spawn new token in place for some points
- get the absolute distance to the exit, though is this tricky because using
  three times will give the exact position of the exit. thus using this skill
  must have a strong impact on the score. some options to explore:

  - an absolute number of points, that increase with each use (eg. 1000, 2000,
    3000, ...)
  - increase the cost of each move by X points -> the later the skill is used,
    the cheaper the cost
  - make it free, the distance to the goal is given along side the distances to
    surrounding walls, the position of the exit is known after 3 moves, but the
    maze make it so it does not matter
  - keep the cost cheap, maybe constant, but give an approximation of the
    distance (~10%?)
