defmodule Maze.View do
  @spec distance(Maze.Board.t(), Maze.coordinates(), Maze.direction()) ::
          {non_neg_integer(), non_neg_integer(), non_neg_integer()}
  def distance(map, coords, direction)

  def distance(map, coords, :north),
    do: {
      distance_straight(map, coords, :west),
      distance_straight(map, coords, :north),
      distance_straight(map, coords, :east)
    }

  def distance(map, coords, :west),
    do: {
      distance_straight(map, coords, :south),
      distance_straight(map, coords, :west),
      distance_straight(map, coords, :north)
    }

  def distance(map, coords, :south),
    do: {
      distance_straight(map, coords, :east),
      distance_straight(map, coords, :south),
      distance_straight(map, coords, :west)
    }

  def distance(map, coords, :east),
    do: {
      distance_straight(map, coords, :north),
      distance_straight(map, coords, :east),
      distance_straight(map, coords, :south)
    }

  @spec distance_straight(Maze.Board.t(), Maze.coordinates(), Maze.direction(), non_neg_integer()) ::
          non_neg_integer()
  defp distance_straight(map, coords, direction, distance \\ 0) do
    if Maze.Board.fetch(map, coords) |> Maze.Cell.wall?(direction) do
      distance
    else
      {:ok, next} = Maze.Move.forward(coords, direction, map)
      distance_straight(map, next, direction, distance + 1)
    end
  end
end
