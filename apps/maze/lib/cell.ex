defmodule Maze.Cell do
  @moduledoc """
  Helpers to manipulate a single cell of a maze.

  The value of a cell depends on the walls that surrounds it. Walls of a cell
  are stored as bit-masks.

  A cell with a value of `0x00` has all its walls open while a cell with a value
  of `0x03` has only the north and west walls closed.
  """

  import Bitwise

  @typedoc "Closed walls of a cell, as a bit mask on 8 bits."
  @type t :: integer()

  @north 0x01
  @west 0x02
  @south 0x04
  @east 0x08

  @deadend <<0xE2, 0x96, 0x91>>
  @wall <<0xE2, 0x96, 0x93>>

  @doc "Give the value for the opposite wall."
  @spec opposite(Maze.direction()) :: Maze.direction()
  def opposite(direction)
  def opposite(:north), do: :south
  def opposite(:south), do: :north
  def opposite(:west), do: :east
  def opposite(:east), do: :west

  @doc """
  Removes the wall in `direction` from a cell.

  ## Parameters

  - cell: Current value of a cell.
  - direction: Direction of the wall to remove.

  ## Example

      iex> Maze.Cell.remove_wall(0x03, :north)
      0x02

      iex> 0x0F |> Maze.Cell.remove_wall(:west) |> Maze.Cell.remove_wall(:south)
      0x09

      iex> 0x0F |> Maze.Cell.remove_wall(:north) |> Maze.Cell.remove_wall(:east)
      0x06

  """
  @spec remove_wall(t(), Maze.direction()) :: t()
  def remove_wall(cell, direction)
  def remove_wall(cell, :north), do: cell &&& (@west ||| @south ||| @east)
  def remove_wall(cell, :west), do: cell &&& (@north ||| @south ||| @east)
  def remove_wall(cell, :south), do: cell &&& (@west ||| @north ||| @east)
  def remove_wall(cell, :east), do: cell &&& (@west ||| @south ||| @north)

  @doc """
  Checks if a cell has a wall in a given direction.

  ## Example

      iex> Maze.Cell.wall?(0x00, :north)
      false

      iex> Maze.Cell.wall?(0x0F, :west)
      true

      iex> Maze.Cell.wall?(0x03, :south)
      false

      iex> Maze.Cell.wall?(0x09, :east)
      true
  """
  @spec wall?(t(), Maze.direction()) :: boolean()
  def wall?(cell, direction)

  def wall?(cell, :north), do: (cell &&& @north) != 0
  def wall?(cell, :west), do: (cell &&& @west) != 0
  def wall?(cell, :south), do: (cell &&& @south) != 0
  def wall?(cell, :east), do: (cell &&& @east) != 0

  @doc """
  Converts 2D coordinates to a 1d point.

  ## Example

      iex> Maze.Cell.to_1d({0, 0}, {10, 20})
      0

      iex> Maze.Cell.to_1d({5, 3}, {10, 20})
      35

  """
  @spec to_1d(Maze.Board.coordinates(), Maze.Board.dimensions()) :: integer()
  def to_1d({x, y}, {width, _height}), do: y * width + x

  @doc """
  Computes the value of a cell surrounded by walls.

      iex> Maze.Cell.closed()
      0x0F
  """
  @spec closed() :: integer()
  def closed do
    @north ||| @west ||| @south ||| @east
  end

  @doc """
  Tests if the coordinates of a cell are within the boundaries of a maze.

  ## Example

      iex> Maze.Cell.valid?({-1, 0}, {10, 20})
      false

      iex> Maze.Cell.valid?({2, -1}, {10, 20})
      false

      iex> Maze.Cell.valid?({120, 5}, {10, 20})
      false

      iex> Maze.Cell.valid?({9, 120}, {10, 20})
      false

      iex> Maze.Cell.valid?({10, 14}, {10, 20})
      false

      iex> Maze.Cell.valid?({9, 20}, {10, 20})
      false

      iex> Maze.Cell.valid?({9, 17}, {10, 20})
      true

  """
  @spec valid?(Maze.coordinates(), Maze.dimensions()) :: boolean()
  def valid?({x, y} = _coordinates, {width, height} = _dimensions) do
    cond do
      x < 0 -> false
      y < 0 -> false
      x >= width -> false
      y >= height -> false
      true -> true
    end
  end

  @doc """
  Returns the text representation of a cell.
  """
  @spec to_char(t()) :: binary()
  def to_char(0), do: <<0xE2, 0x95, 0xAC>>
  def to_char(@north), do: <<0xE2, 0x95, 0xA6>>
  def to_char(@west), do: <<0xE2, 0x95, 0xA0>>
  def to_char(@south), do: <<0xE2, 0x95, 0xA9>>
  def to_char(@east), do: <<0xE2, 0x95, 0xA3>>

  def to_char(cell) when (@north ||| @south) === cell, do: <<0xE2, 0x95, 0x90>>
  def to_char(cell) when (@west ||| @east) === cell, do: <<0xE2, 0x95, 0x91>>
  def to_char(cell) when (@north ||| @west) === cell, do: <<0xE2, 0x95, 0x94>>
  def to_char(cell) when (@north ||| @east) === cell, do: <<0xE2, 0x95, 0x97>>
  def to_char(cell) when (@west ||| @south) === cell, do: <<0xE2, 0x95, 0x9A>>
  def to_char(cell) when (@south ||| @east) === cell, do: <<0xE2, 0x95, 0x9D>>

  def to_char(cell) when (@north ||| @east ||| @south) === cell,
    do: @deadend

  def to_char(cell) when (@north ||| @west ||| @south) === cell,
    do: @deadend

  def to_char(cell) when (@west ||| @east ||| @south) === cell,
    do: @deadend

  def to_char(cell) when (@north ||| @west ||| @east) === cell,
    do: @deadend

  def to_char(cell)
      when (@north ||| @west ||| @east ||| @south) === cell,
      do: @wall
end
