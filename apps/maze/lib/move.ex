defmodule Maze.Move do
  @doc "Rotates left."
  @spec left(Maze.direction()) :: Maze.direction()
  def left(:north), do: :west
  def left(:west), do: :south
  def left(:south), do: :east
  def left(:east), do: :north

  @doc "Rotates right."
  @spec right(Maze.direction()) :: Maze.direction()
  def right(:north), do: :east
  def right(:west), do: :north
  def right(:south), do: :west
  def right(:east), do: :south

  @doc "Moves forward one cell."
  @spec forward(Maze.coordinates(), Maze.direction(), Maze.Board.t()) ::
          {:ok | :error, Maze.coordinates()}
  def forward(coordinates, direction, map)

  def forward({x, y} = coordinates, :north, map),
    do: try_move({x, y - 1}, coordinates, map, :north)

  def forward({x, y} = coordinates, :west, map),
    do: try_move({x - 1, y}, coordinates, map, :west)

  def forward({x, y} = coordinates, :south, map),
    do: try_move({x, y + 1}, coordinates, map, :south)

  def forward({x, y} = coordinates, :east, map),
    do: try_move({x + 1, y}, coordinates, map, :east)

  @spec try_move(Maze.coordinates(), Maze.coordinates(), Maze.Board.t(), Maze.direction()) ::
          {:ok | :error, Maze.coordinates()}
  defp try_move(new, old, map, direction) do
    case Maze.Cell.valid?(new, map.size) do
      false ->
        {:error, old}

      true ->
        case Maze.Board.fetch(map, old) |> Maze.Cell.wall?(direction) do
          false -> {:ok, new}
          true -> {:error, old}
        end
    end
  end
end
