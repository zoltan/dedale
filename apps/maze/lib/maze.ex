defmodule Maze do
  @moduledoc """
  Top level module for maze generation and manipulation.
  """

  @typedoc "2d coordinates in a maze."
  @type coordinates :: {non_neg_integer(), non_neg_integer()}

  @typedoc "Dimensions of a maze, height and width."
  @type dimensions :: {non_neg_integer(), non_neg_integer()}

  @typedoc "One dimension representation of a maze."
  @type map1d :: <<_::8>>

  @typedoc "Direction when on a cell."
  @type direction :: :north | :west | :south | :east | :unknown
end
