defmodule Maze.Board do
  @moduledoc """
  Defines and generates a board.
  """

  defstruct size: {0, 0},
            map: <<>>,
            start_pos: {0, 0},
            final_pos: {0, 0},
            distance: 0,
            seed: {0, 0}

  @typedoc "Structure holding board information."
  @type t :: %__MODULE__{}

  @doc """
  Creates a new maze.

  ## Parameter

  - size: Width and height of the maze to generate, as a pair.
  """
  @spec new(Maze.dimensions()) :: t()
  def new(size) do
    # Force the random generator initialization otherwise the seed may
    # be :undefined.
    _ = :rand.uniform()
    {:exsss, [seed | jump]} = :rand.export_seed()

    %{
      backtrack(%__MODULE__{
        size: size,
        start_pos: start_point(size),
        map: blank_map(size)
      })
      | seed: {seed, jump}
    }
  end

  @doc """
  Gets the value of a cell at coordinates.
  """
  @spec fetch(t(), Maze.coordinates()) :: Maze.Cell.t()
  def fetch(map, {_x, _y} = coords) do
    if Maze.Cell.valid?(coords, map.size) do
      offset = Maze.Cell.to_1d(coords, map.size)
      <<_head::binary-size(offset), cell, _tail::binary>> = map.map
      cell
    end
  end

  @doc """
  Creates a maze from a given seed.

  ## Parameter

  - size: Width and height of the maze to generate, as a pair.
  - seed: Pair of number used to seed the random generator with `:rand.seed/1`.

  ## Example

      iex> Maze.Board.new({2, 3}, {18004725169209611, 18723378024040426})
      %Maze.Board{
        distance: 5,
        final_pos: {0, 1},
        map: <<3, 9, 14, 10, 7, 12>>,
        seed: {18004725169209611, 18723378024040426},
        size: {2, 3},
        start_pos: {0, 2}
      }

  """
  @spec new(Maze.dimensions(), {non_neg_integer(), non_neg_integer()}) :: t()
  def new(size, {seed, jump} = state) do
    :rand.seed({:exsss, [seed | jump]})

    %{
      backtrack(%__MODULE__{
        size: size,
        start_pos: start_point(size),
        map: blank_map(size)
      })
      | seed: state
    }
  end

  @spec blank_map(Maze.dimensions()) :: binary()
  defp blank_map({width, height}) do
    :binary.list_to_bin(Enum.take(Stream.cycle([Maze.Cell.closed()]), width * height))
  end

  @spec start_point(Maze.dimensions()) :: Maze.coordinates()
  defp start_point({width, height}) do
    {floor(:rand.uniform() * width), floor(:rand.uniform() * height)}
  end

  @spec neighbors(Maze.coordinates(), Maze.dimensions()) :: []
  defp neighbors({x, y}, mapsize) do
    [{:north, {x, y - 1}}, {:west, {x - 1, y}}, {:south, {x, y + 1}}, {:east, {x + 1, y}}]
    |> Enum.shuffle()
    |> Enum.filter(&Maze.Cell.valid?(elem(&1, 1), mapsize))
  end

  @spec visited?(pid(), Maze.coordinates()) :: boolean()
  defp visited?(pid, coords) do
    Agent.get(pid, fn x -> MapSet.member?(x, coords) end)
  end

  @spec links_to_binary(Maze.dimensions(), [{Maze.direction(), Maze.coordinates()}], binary()) ::
          binary()
  defp links_to_binary(_size, [], map), do: map

  defp links_to_binary(size, links, map) do
    [
      {direction, coords}
      | tail
    ] = links

    links_to_binary(
      size,
      tail,
      update_map(map, Maze.Cell.to_1d(coords, size), direction)
    )
  end

  @spec update_map(Maze.map1d(), non_neg_integer(), Maze.direction()) ::
          Maze.map1d()
  defp update_map(map, 0, direction) do
    <<cell, tail::binary>> = map
    <<Maze.Cell.remove_wall(cell, direction)>> <> tail
  end

  defp update_map(map, offset, direction) do
    <<head::binary-size(offset), cell, tail::binary>> = map
    head <> <<Maze.Cell.remove_wall(cell, direction)>> <> tail
  end

  @spec backtrack(t()) :: t()
  defp backtrack(board) do
    {:ok, visited} = Agent.start_link(fn -> MapSet.new() end)

    {distance, final_pos, links} =
      backtrack(board.size, board.start_pos, visited, 0, {:unknown, board.start_pos})

    Agent.stop(visited)

    %{
      board
      | distance: distance,
        final_pos: final_pos,
        map:
          links_to_binary(
            board.size,
            links,
            board.map
          )
    }
  end

  @spec backtrack(
          Maze.dimensions(),
          Maze.coordinates(),
          pid(),
          non_neg_integer(),
          {Maze.direction(), Maze.coordinates()}
        ) :: {integer(), Maze.coordinates(), [{Maze.direction(), Maze.coordinates()}]}
  defp backtrack(size, coords, visited, distance, {prev_dir, prev_coords} = parent) do
    if visited?(visited, coords) do
      {distance - 1, prev_coords, []}
    else
      :ok = Agent.update(visited, fn x -> MapSet.put(x, coords) end)

      child_path =
        neighbors(coords, size)
        |> Enum.map(fn {direction, next_coords} ->
          backtrack(
            size,
            next_coords,
            visited,
            distance + 1,
            {direction, coords}
          )
        end)

      links =
        case prev_dir do
          :unknown -> []
          _ -> [parent, {Maze.Cell.opposite(prev_dir), coords}]
        end

      farest_point =
        child_path
        |> Enum.map(fn x -> {elem(x, 0), elem(x, 1)} end)
        |> Enum.max_by(&elem(&1, 0))

      Tuple.append(
        farest_point,
        links ++
          (child_path
           |> Enum.map(&elem(&1, 2))
           |> Enum.filter(fn x -> List.first(x) != [] end)
           |> Enum.reduce(fn x, acc -> acc ++ x end))
      )
    end
  end

  @doc """
  Helper function to show a text representation of a maze.
  """
  @spec inspect(t()) :: t()
  def inspect(map) do
    chars = for <<c::8 <- map.map>>, do: Maze.Cell.to_char(c)
    {width, _height} = map.size
    chars |> Enum.chunk_every(width) |> Enum.map(&Enum.join/1) |> Enum.map(&IO.puts/1)
    map
  end
end
