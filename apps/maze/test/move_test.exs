defmodule MazeMoveTest do
  use ExUnit.Case
  doctest Maze.Move

  setup do
    %{map: Maze.Board.new({3, 2}, {146_572_290_110_197_062, 257_860_924_907_848_967})}
  end

  test "Forward into wall", context do
    assert Maze.Move.forward({0, 0}, :north, context[:map]) == {:error, {0, 0}}
    assert Maze.Move.forward({1, 1}, :south, context[:map]) == {:error, {1, 1}}
    assert Maze.Move.forward({2, 1}, :east, context[:map]) == {:error, {2, 1}}
  end

  test "Forward ok", context do
    assert Maze.Move.forward({0, 0}, :south, context[:map]) == {:ok, {0, 1}}
    assert Maze.Move.forward({1, 1}, :west, context[:map]) == {:ok, {0, 1}}
    assert Maze.Move.forward({2, 1}, :north, context[:map]) == {:ok, {2, 0}}
  end
end
