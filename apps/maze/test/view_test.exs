defmodule MazeViewTest do
  use ExUnit.Case

  setup do
    %{map: Maze.Board.new({5, 5}, {46_521_950_153_386_103, 203_551_539_295_947_262})}
  end

  test "Distance to walls", context do
    assert Maze.View.distance(context[:map], {3, 2}, :north) == {2, 0, 0}
    assert Maze.View.distance(context[:map], {3, 2}, :west) == {0, 2, 0}
    assert Maze.View.distance(context[:map], {3, 2}, :south) == {0, 0, 2}
    assert Maze.View.distance(context[:map], {3, 2}, :east) == {0, 0, 0}

    assert Maze.View.distance(context[:map], {0, 1}, :north) == {0, 1, 3}
    assert Maze.View.distance(context[:map], {0, 1}, :west) == {1, 0, 1}
    assert Maze.View.distance(context[:map], {0, 1}, :south) == {3, 1, 0}
    assert Maze.View.distance(context[:map], {0, 1}, :east) == {1, 3, 1}
  end
end
