defmodule MazeBoardTest do
  use ExUnit.Case
  doctest Maze.Board

  setup do
    %{map: Maze.Board.new({3, 2}, {146_572_290_110_197_062, 257_860_924_907_848_967})}
  end

  test "fetch valid coordinates", context do
    assert Maze.Board.fetch(context[:map], {0, 0}) === 11
    assert Maze.Board.fetch(context[:map], {2, 1}) === 14
  end

  test "fetch invalid coordinates", context do
    assert Maze.Board.fetch(context[:map], {-1, 0}) === nil
    assert Maze.Board.fetch(context[:map], {0, -1}) === nil
    assert Maze.Board.fetch(context[:map], {10, 10}) === nil
  end
end
