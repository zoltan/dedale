defmodule Dedale.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      version: "0.1.0",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Dedale",
      docs: [
        main: "readme",
        source_url: "https://gitlab.com/zoltan/dedale",
        extras: ["README.md"]
      ]
    ]
  end

  # Dependencies listed here are available only for this
  # project and cannot be accessed from applications inside
  # the apps folder.
  #
  # Run "mix help deps" for examples and options.
  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev},
      {:dialyxir, "~> 0.5", only: [:dev]}
    ]
  end
end
